let posts = [];
let count = 1;

// ADD POST DATA

document.querySelector("#form-add-post").addEventListener('submit',(event) =>{
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});                                                     
	
	count++;

	showPost(posts);
	alert("Succesfully added post.");                                
})



const showPost = (post) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}


//Edit Post


const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;


	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-title-edit").value = title;
	document.querySelector("#txt-body-edit").value = body;
}


//UPDATE POST

document.querySelector("#form-edit-post").addEventListener('submit',(e) =>{
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-title-edit").value;
			posts[i].body = document.querySelector("#txt-body-edit").value;


			showPost(posts);
			alert("Succesfully Updated");

			break;
		}
	}
})


const deletePost = (id) => {
	posts = posts.filter((post) => {
		if (post.id.toString() !== id){
			return post;
		}
	})
	document.querySelector(`#post-${id}`).remove();
}